package kiloboltgame;

import java.awt.Image;
import java.awt.Rectangle;

public class Tile {
    public final static int TILE_TYPE_DIRT = 5;
    public final static int TILE_TYPE_GRASS_TOP = 8;
    public final static int TILE_TYPE_GRASS_LEFT = 4;
    public final static int TILE_TYPE_GRASS_RIGHT = 6;
    public final static int TILE_TYPE_GRASS_BOT = 2;
    public final static int TILE_TYPE_EMPTY = 0;

    private int tileX, tileY, speedX, type;
    public Image tileImage;

    private Robot robot = StartingClass.getRobot();
    private Background bg = StartingClass.getBg1();
    
    private Rectangle r;

    public Tile(int x, int y, int typeInt) {
        tileX = x * 40;
        tileY = y * 40;

        type = typeInt;
        
        r = new Rectangle();

        if (type == TILE_TYPE_DIRT) {
            tileImage = StartingClass.tiledirt;
        } else if (type == TILE_TYPE_GRASS_TOP) {
            tileImage = StartingClass.tilegrassTop;
        } else if (type == TILE_TYPE_GRASS_LEFT) {
            tileImage = StartingClass.tilegrassLeft;
        } else if (type == TILE_TYPE_GRASS_RIGHT) {
            tileImage = StartingClass.tilegrassRight;
        } else if (type == TILE_TYPE_GRASS_BOT) {
            tileImage = StartingClass.tilegrassBot;
        } else {
            type = TILE_TYPE_EMPTY;
        }

    }

    public void update() {
        speedX = bg.getSpeedX() * 5;
        tileX += speedX;
        r.setBounds(tileX, tileY, 40, 40);
        
        if (r.intersects(Robot.yellowRed) && type != 0) {
            checkVerticalCollision(Robot.rectBottom, Robot.rectTop);
            checkSideCollision(robot.rectLeftHand, robot.rectRightHand, robot.rectFootLeft, robot.rectFootRight);
        }
    }

    public int getTileX() {
        return tileX;
    }

    public void setTileX(int tileX) {
        this.tileX = tileX;
    }

    public int getTileY() {
        return tileY;
    }

    public void setTileY(int tileY) {
        this.tileY = tileY;
    }

    public Image getTileImage() {
        return tileImage;
    }

    public void setTileImage(Image tileImage) {
        this.tileImage = tileImage;
    }
    
    public void checkVerticalCollision(Rectangle rtop, Rectangle rbot) {
        if (rtop.intersects(r)) {
            
        }

        if (rbot.intersects(r) && type == TILE_TYPE_GRASS_TOP) {
            robot.setJumped(false);
            robot.setSpeedY(0);
            robot.setCenterY(tileY - 63);
        }
    }
    
    public void checkSideCollision(Rectangle rleft, Rectangle rright, Rectangle leftfoot, Rectangle rightfoot) {
        if (type != TILE_TYPE_DIRT && type != TILE_TYPE_GRASS_BOT && type != TILE_TYPE_EMPTY){
            if (rleft.intersects(r)) {
                robot.setCenterX(tileX + 102); 
                robot.setSpeedX(0);
            }else if (leftfoot.intersects(r)) {
                robot.setCenterX(tileX + 85);
                robot.setSpeedX(0);
            }
            
            if (rright.intersects(r)) {
                robot.setCenterX(tileX - 62);
                robot.setSpeedX(0);
            }
            else if (rightfoot.intersects(r)) {
                robot.setCenterX(tileX - 45);
                robot.setSpeedX(0);
            }
        }
    }
}